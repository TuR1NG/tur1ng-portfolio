import React from 'react';

import classes from '../sass/pages/Me.module.scss';

const Me = () => {
	return (
		<div className={classes.container}>
			<div className={classes.ropeStart}></div>
			<div className={classes.textContainer}>
				<h1 className={classes.title}>Mohammad ali Ali panah</h1>
				<h3 className={classes.intro}>
					My name is Mohammad ali Ali panah. 19 years old Full Stack Web Developer based
					in Tehran, Iran.
					<br />
					Currently working as junior React developer.
				</h3>
			</div>
			<img
				className={classes.scrollgif}
				src={require('../assets/images/scrolling_mousewheel.gif')}
				alt="scroll"
			/>
			<div className={classes.ropeEnd}></div>
		</div>
	);
};

export default Me;
