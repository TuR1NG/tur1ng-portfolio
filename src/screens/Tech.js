import React from 'react';
import { useInView } from 'react-intersection-observer';

import { cardContent } from '../helpers/CardCotent';
import { TechIcons } from '../helpers/TechIcons';
import classes from './Tech.module.css';

const Card = (props) => {
	return (
		<div className={classes.card}>
			<h2>{cardContent[props.which].title}</h2>
			<h4 className={classes.textBody}>{cardContent[props.which].body}</h4>
		</div>
	);
};

const Tech = () => {
	const [ref, inView] = useInView({
		threshold: 0,
		rootMargin: '-150px',
		triggerOnce: true,
	});

	return (
		<div ref={ref} className={classes.container}>
			<div className={classes.cartContainer}>
				<Card which={0} />
				<Card which={1} />
			</div>
			<div className={classes.rope}>
				<div className={classes.roller}>
					{TechIcons.map((icon, index) => {
						return (
							<img
								key={`${index}-${icon}`}
								style={{ width: '70%' }}
								src={require(`../${icon.imageSrc}`)}
								alt={icon.alt}
							/>
						);
					})}
				</div>
			</div>
			<div className={classes.cartContainer}>
				<Card which={2} />
				<Card which={3} />
			</div>
		</div>
	);
};

export default Tech;
