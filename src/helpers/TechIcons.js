const iconsDir = 'assets/images/icons';

export const TechIcons = [
	{ imageSrc: `${iconsDir}/apollo.png`, alt: 'apollo' },
	{ imageSrc: `${iconsDir}/docker.png`, alt: 'docker' },
	{ imageSrc: `${iconsDir}/graphql.png`, alt: 'graphql' },
	{ imageSrc: `${iconsDir}/kubernetes.png`, alt: 'kubernetes' },
	{ imageSrc: `${iconsDir}/mongodb.png`, alt: 'mongodb' },
	{ imageSrc: `${iconsDir}/nodejs.png`, alt: 'nodejs' },
	{ imageSrc: `${iconsDir}/postgresql.png`, alt: 'postgresql' },
	{ imageSrc: `${iconsDir}/react.png`, alt: 'react' },
	{ imageSrc: `${iconsDir}/redis.png`, alt: 'redis' },
	{ imageSrc: `${iconsDir}/typescript.png`, alt: 'typescript' },
];
