export const cardContent = [
	{
		title: 'well-architected, readable, and reusable',
		body:
			'Lorem eiusmod incididunt laboris id labore minim quis ullamco commodo dolor. Qui exercitation cillum ullamco laboris cupidatat qui excepteur ipsum qui culpa ut proident elit. Sunt nulla ad consectetur laborum et incididunt dolor pariatur in excepteur commodo Lorem et occaecat. Irure proident quis duis nulla amet. Veniam aliqua irure elit pariatur dolore cupidatat voluptate laborum dolor. Laboris laborum minim incididunt sint labore in mollit.',
	},
	{
		title: 'from design to code',
		body:
			'Lorem eiusmod incididunt laboris id labore minim quis ullamco commodo dolor. Qui exercitation cillum ullamco laboris cupidatat qui excepteur ipsum qui culpa ut proident elit. Sunt nulla ad consectetur laborum et incididunt dolor pariatur in excepteur commodo Lorem et occaecat. Irure proident quis duis nulla amet. Veniam aliqua irure elit pariatur dolore cupidatat voluptate laborum dolor. Laboris laborum minim incididunt sint labore in mollit.',
	},
	{
		title: 'Full Stack Developer',
		body:
			'Lorem eiusmod incididunt laboris id labore minim quis ullamco commodo dolor. Qui exercitation cillum ullamco laboris cupidatat qui excepteur ipsum qui culpa ut proident elit. Sunt nulla ad consectetur laborum et incididunt dolor pariatur in excepteur commodo Lorem et occaecat. Irure proident quis duis nulla amet. Veniam aliqua irure elit pariatur dolore cupidatat voluptate laborum dolor. Laboris laborum minim incididunt sint labore in mollit.',
	},
	{
		title: 'Test and Deploy',
		body:
			'Lorem eiusmod incididunt laboris id labore minim quis ullamco commodo dolor. Qui exercitation cillum ullamco laboris cupidatat qui excepteur ipsum qui culpa ut proident elit. Sunt nulla ad consectetur laborum et incididunt dolor pariatur in excepteur commodo Lorem et occaecat. Irure proident quis duis nulla amet. Veniam aliqua irure elit pariatur dolore cupidatat voluptate laborum dolor. Laboris laborum minim incididunt sint labore in mollit.',
	},
];
