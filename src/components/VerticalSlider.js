import React from 'react';
import { Element, scroller, Events } from 'react-scroll';

import { Slides } from '../helpers/Slides.js';
import Me from '../screens/Me';
import Tech from '../screens/Tech';
import Project from '../screens/Project';
import Contact from '../screens/Contact';

import classes from '../sass/component/VerticalSlider.module.scss';

let scrolling = false;
const VerticalSlider = (props) => {
	// scroll whenever clicked on navigattion
	scroller.scrollTo(props.selectedSlide.slideName, { duration: 250, smooth: true });

	Events.scrollEvent.register('begin', () => {
		if (!scrolling) {
			scrolling = true;
		}
	});
	Events.scrollEvent.register('end', () => {
		if (scrolling) {
			scrolling = false;
		}
	});

	return (
		<div
			onWheel={(event) => {
				if (!scrolling) {
					// check the direction of scroll
					if (event.nativeEvent.wheelDelta > 0) {
						let previousSlide;
						for (var i = 0; i < Slides.length; i++) {
							if (props.selectedSlide.slideIndex === 1) {
								return; // return if current slide is first slide. to prevent loop
							} else if (Slides[i].slideIndex === props.selectedSlide.slideIndex) {
								previousSlide = Slides[i - 1];
							}
						}
						if (previousSlide) {
							props.onSlideChange(previousSlide);
							scroller.scrollTo(previousSlide.slideName, {
								duration: 250,
								smooth: true,
							});
						}
					} else {
						let nextSlide;
						for (let j = 0; j < Slides.length; j++) {
							if (props.selectedSlide.slideIndex === 4) {
								return; // return if current slide is last slide. to prevent loop
							} else if (Slides[j].slideIndex === props.selectedSlide.slideIndex) {
								nextSlide = Slides[j + 1];
							}
						}
						if (nextSlide) {
							props.onSlideChange(nextSlide);
							scroller.scrollTo(nextSlide.slideName, {
								duration: 250,
								smooth: true,
							});
						}
					}
				}
			}}
		>
			<Element name="TuR1NG" className={classes.slide}>
				<Me />
			</Element>
			<Element name="Technologies" className={classes.slide}>
				<Tech />
			</Element>
			<Element name="Projects" className={classes.slide}>
				<Project />
			</Element>
			<Element name="Contact-Me" className={classes.slide}>
				<Contact />
			</Element>
		</div>
	);
};

export default VerticalSlider;
