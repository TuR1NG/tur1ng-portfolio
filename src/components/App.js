import React, { useState } from 'react';

import VerticalSwiper from './VerticalSlider';
import SlideNavigator from './SlideNavigator';

const App = () => {
	const [currentSlide, setCurrentSlide] = useState({ slideName: 'TuR1NG', slideIndex: 1 });
	// set the current slide to one that is selected with navigation
	const onSelectHandler = (slide) => {
		setCurrentSlide(slide);
	};
	// update current slide when scroll happend
	const slideChangeHandler = (slide) => {
		setCurrentSlide(slide);
	};

	return (
		<>
			<VerticalSwiper selectedSlide={currentSlide} onSlideChange={slideChangeHandler} />
			<SlideNavigator currentSlide={currentSlide} onSelect={onSelectHandler} />
		</>
	);
};

export default App;
