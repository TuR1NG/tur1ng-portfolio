import React from 'react';

import { Slides } from '../helpers/Slides';
import classes from '../sass/component/SlideNavigator.module.scss';

const SlideNavigator = (props) => {
	const currentSlide = props.currentSlide.slideName;
	return (
		<div className={classes.content}>
			{Slides.map((slide, index) => {
				return (
					<div
						style={{
							backgroundColor: currentSlide === slide.slideName ? '#000' : null,
						}}
						key={index}
						onClick={() => props.onSelect(slide)}
						className={classes.slideIcon}
					></div>
				);
			})}
		</div>
	);
};

export default SlideNavigator;
